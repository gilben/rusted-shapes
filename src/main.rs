extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;

use piston::window::WindowSettings;
use piston::event_loop::*;
use piston::input::*;
use glutin_window::GlutinWindow as Window;
use opengl_graphics::{ GlGraphics, OpenGL };

pub struct App {
    gl: GlGraphics, // OpenGL drawing backend
    rotation: f64 // rotation for the square
}

impl App {
    fn render(&mut self, args: &RenderArgs) {
        use graphics::*;

        const GREEN:    [f32; 4] = [0.0, 1.0, 0.0, 1.0];
        const RED:      [f32; 4] = [1.0, 0.0, 0.0, 1.0];

        // Defining shapes
        let square = rectangle::square(0.0, 0.0, 50.0);
        let square_two = rectangle::square(0.0, 0.0, 20.0);

        // Defining rotation
        let rotation = self.rotation;
        
        // Defining location
        let (x1, y1) = (args.width / 2.0,
                        args.height / 2.0);
        let (x2, y2) = (args.width / 3.0,
                        args.height / 2.0); 
        
        self.gl.draw(args.viewport(), |c, gl|{
            clear(GREEN, gl);

            let transform = c.transform.trans(x1, y1)
                                        .rot_rad(rotation)
                                        .trans(-25.0, -25.0);
            let transform2 = c.transform.trans(x2, y2);
            rectangle(RED, square, transform,gl);
            rectangle(RED, square_two, transform2, gl);
        });
    }

    fn update(&mut self, args: &UpdateArgs) {
        // rotate
        self.rotation += 2.0 * args.dt;
    }
}

fn main() {
    let opengl = OpenGL::V3_2;

    // Create a Glutin Window
    let mut window: Window = WindowSettings::new(
            "spinning-square",
            [200, 200]
        )
        .opengl(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();

    // Create game and run it
    let mut app = App {
        gl: GlGraphics::new(opengl),
        rotation: 0.0
    };

    let mut events = Events::new(EventSettings::new());
    while let Some(e) = events.next(&mut window) {
        if let Some(r) = e.render_args() {
            app.render(&r);
        }

        if let Some(u) = e.update_args() {
            app.update(&u);
        }
    }    
}